<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html xmlns:th="http://www.thymeleaf.org"
	xmlns:tiles="http://www.thymeleaf.org">
<head>
<meta charset="utf-8">
<title>Ra��es - DanBred</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link
	href="//netdna.bootstrapcdn.com/bootstrap/3.1.0/css/bootstrap.min.css"
	rel="stylesheet" id="bootstrap-css">
<style type="text/css">
body {
	background: #eee;
	/*url(http://subtlepatterns.com/patterns/sativa.png);*/
}

html, body {
	position: relative;
	height: 100%;
}

.login-container {
	position: relative;
	width: 300px;
	margin: 80px auto;
	padding: 20px 40px 40px;
	text-align: center;
	background: #fff;
	border: 1px solid #ccc;
}

#output {
	position: absolute;
	width: 300px;
	top: -75px;
	left: 0;
	color: #fff;
}

#output.alert-success {
	background: rgb(25, 204, 25);
}

#output.alert-danger {
	background: rgb(228, 105, 105);
}

.login-container::before, .login-container::after {
	content: "";
	position: absolute;
	width: 100%;
	height: 100%;
	top: 3.5px;
	left: 0;
	background: #fff;
	z-index: -1;
	-webkit-transform: rotateZ(4deg);
	-moz-transform: rotateZ(4deg);
	-ms-transform: rotateZ(4deg);
	border: 1px solid #ccc;
}

.login-container::after {
	top: 5px;
	z-index: -2;
	-webkit-transform: rotateZ(-2deg);
	-moz-transform: rotateZ(-2deg);
	-ms-transform: rotateZ(-2deg);
}

.avatar {
	width: 100px;
	height: 100px;
	margin: 10px auto 30px;
	border-radius: 100%;
	border: 2px solid #aaa;
	background-size: cover;
	background-image: url('http://www.db.agr.br/views/imgs/db_genetica.png');
	/*${pageContext.request.contextPath}/img/logodb.png');*/
}

.form-box input {
	width: 100%;
	padding: 10px;
	text-align: center;
	height: 40px;
	border: 1px solid #ccc;;
	background: #fafafa;
	transition: 0.2s ease-in-out;
}

.form-box input:focus {
	outline: 0;
	background: #eee;
}

.form-box input[type="text"] {
	border-radius: 5px 5px 0 0;
	text-transform: lowercase;
}

.form-box input[type="password"] {
	border-radius: 0 0 5px 5px;
	border-top: 0;
}

.form-box button.login {
	margin-top: 15px;
	padding: 10px 20px;
}

.animated {
	-webkit-animation-duration: 1s;
	animation-duration: 1s;
	-webkit-animation-fill-mode: both;
	animation-fill-mode: both;
}

@
-webkit-keyframes fadeInUp { 0% {
	opacity: 0;
	-webkit-transform: translateY(20px);
	transform: translateY(20px);
}

100%
{
opacity
:
 
1;
-webkit-transform
:
 
translateY
(0);

    
transform
:
 
translateY
(0);

  
}
}
@
keyframes fadeInUp { 0% {
	opacity: 0;
	-webkit-transform: translateY(20px);
	-ms-transform: translateY(20px);
	transform: translateY(20px);
}

100%
{
opacity
:
 
1;
-webkit-transform
:
 
translateY
(0);

    
-ms-transform
:
 
translateY
(0);

    
transform
:
 
translateY
(0);

  
}
}
.fadeInUp {
	-webkit-animation-name: fadeInUp;
	animation-name: fadeInUp;
}
</style>
<script src="//code.jquery.com/jquery-1.10.2.min.js"></script>
<script
	src="//netdna.bootstrapcdn.com/bootstrap/3.1.0/js/bootstrap.min.js"></script>
<script type="text/javascript">
	window.alert = function() {
	};
	var defaultCSS = document.getElementById('bootstrap-css');
	function changeCSS(css) {
		if (css)
			$('head > link')
					.filter(':first')
					.replaceWith(
							'<link rel="stylesheet" href="'+ css +'" type="text/css" />');
		else
			$('head > link').filter(':first').replaceWith(defaultCSS);
	}
</script>
</head>
<body>
	<c:if test="${not empty SPRING_SECURITY_LAST_EXCEPTION}">
		<font color="red">
			<c:if test="${SPRING_SECURITY_LAST_EXCEPTION.message eq 'Bad credentials.' }">
				<p>Usu�rio ou senha incorreto.</p>
			</c:if>
			<c:if test="${SPRING_SECURITY_LAST_EXCEPTION.message != 'Bad credentials.' }">
				Your login attempt was not successful due to <c:out value="${SPRING_SECURITY_LAST_EXCEPTION.message}" />.
			</c:if>
			
			
		</font>
	</c:if>
	<div class="container">
		<div class="login-container">
			<div id="output"></div>
			<div class="avatar"></div>
			<div class="form-box">
				<form name="f" th:action="@{/login}" method="post" id="formulario">
					<fieldset>
						<legend>Digite seu usu�rio e senha</legend>
						<label for="username">Usu�rio</label> <input type="text"
							id="username" name="username" /> <label for="password">Senha</label>
						<input type="password" id="password" name="password" /> <input
							type="hidden" name="${_csrf.parameterName}"
							value="${_csrf.token}" />
						<div class="form-actions">
							<button type="submit" class="btn">Entrar</button>
						</div>
					</fieldset>
				</form>
			</div>
		</div>
	</div>

	<script type="text/javascript">
		$(function() {
			var textfield = $("input[name=username]");
			$('button[type="submit"]').click(
					function(e) {
						e.preventDefault();
						//little validation just to check username
						if (textfield.val() != "") {
							//$("body").scrollTo("#output");

							$("#output").addClass(
									"alert alert-success animated fadeInUp")
									.html("Carregando...");
							$("#output").removeClass(' alert-danger');
							$("input").css({
								"height" : "0",
								"padding" : "0",
								"margin" : "0",
								"opacity" : "0"
							});
							//change button text 
							$('button[type="submit"]').html("continue")
									.removeClass("btn-info").addClass(
											"btn-default").click(function() {
										$("input").css({
											"height" : "auto",
											"padding" : "10px",
											"opacity" : "1"
										}).val("");
									});

						} else {
							//remove success mesage replaced with error message
							$("#output").removeClass(' alert alert-success');
							$("#output").addClass(
									"alert alert-danger animated fadeInUp")
									.html("Favor digitar o usuрrio.");
						}
						$("#formulario").submit();
					});
		});
	</script>
</body>
</html>