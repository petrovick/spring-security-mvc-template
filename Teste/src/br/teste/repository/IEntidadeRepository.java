package br.teste.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import br.teste.business.Entidade;


public interface IEntidadeRepository extends JpaRepository<Entidade, Integer>
{
	public List<Entidade> findByNomeEntidadeContaining(String entidade);
}

