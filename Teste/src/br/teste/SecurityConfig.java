package br.teste;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

	@Autowired
	DataSource dataSource;
	
	@Autowired
	 public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
	    	auth
      .jdbcAuthentication()
          .dataSource(dataSource)
          //.withDefaultSchema()
          .usersByUsernameQuery("select username,password, enabled from users where username=?")
          .authoritiesByUsernameQuery("select ur.USUARIOS_USERNAME, pa.DESCRICAOAUTENTICACAO from PAPEL_USERS ur join PAPEL pa on pa.IDPAPEL = ur.PAPEL_IDPAPEL where ur.USUARIOS_USERNAME=?");
	    	
	 }
	

	 @Override
	 protected void configure(HttpSecurity http) throws Exception
	 {
		 
		 http.authorizeRequests()
			 .antMatchers("/resources/**").permitAll()
			 .antMatchers("/service/**").permitAll()
	
			 
			 //.antMatchers("/").access("hasAnyRole('ROLE_USER', 'ROLE_ADMIN')")
			.antMatchers("/entidade/**").access("hasAnyRole('Usuário Padrão', 'Administrador', 'Master')")
			.antMatchers("/relatorio/**").access("hasAnyRole('Usuário Padrão', 'Administrador', 'Master')")
			.antMatchers("/unidade/**").access("hasRole('Master')")
			.antMatchers("/categoria/**").access("hasRole('Master')")
			.antMatchers("/grafico/**").access("hasAnyRole('Usuário Padrão', 'Administrador', 'Master')")
			.antMatchers("/cadastro/produto/**").access("hasAnyRole('Usuário Padrão', 'Administrador', 'Master')")
			
			 .antMatchers("/**").authenticated()
			 
		.and()
			.exceptionHandling().accessDeniedPage("/403")
		    
		.and()
			.logout()
			.logoutRequestMatcher(new AntPathRequestMatcher("/logout"))
			.logoutSuccessUrl("/login")
			
		.and()
			.formLogin().loginPage("/login").failureUrl("/login?error").permitAll()

		.and()
		.csrf()
              .disable();
			
			//.formLogin();//.loginPage("/login");
	 }

}
