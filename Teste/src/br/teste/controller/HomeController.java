package br.teste.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class HomeController
{

	@RequestMapping("/")
	public ModelAndView index()
	{
		ModelAndView model = new ModelAndView("/privado/index");
		return model;
	}

	

}
