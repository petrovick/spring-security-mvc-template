package br.teste.business;

import java.math.BigInteger;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity()
@Table(name = "ENTIDADE")
public class Entidade
{
	@Id
	@SequenceGenerator(sequenceName="IDENTIDADESEQUENCE", name = "IDENTIDADESEQUENCE", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "IDENTIDADESEQUENCE")
	@Column(name = "IDENTIDADE", unique = true, nullable = false)
	private Integer idEntidade;
	
	@Column(name = "NOMEENTIDADE", length = 30)
	private String nomeEntidade;

	@Column(name = "NOMEFANTASIA", length = 30)
	private String nomeFantasia;

	@Column(name = "CGCCPF")
	private BigInteger Cgccpf;

	@Column(name = "INSCESTADUAL")
	private BigInteger inscEstadual;
	
	public Entidade(){}
	
	public Entidade(Integer idEntidade)
	{
		this.idEntidade = idEntidade;
	}
	
	

	public Entidade(Integer idEntidade, String nomeEntidade, String nomeFantasia, 
			BigInteger cgccpf,
			BigInteger inscEstadual) {
		super();
		this.idEntidade = idEntidade;
		this.nomeEntidade = nomeEntidade;
		this.nomeFantasia = nomeFantasia;
		Cgccpf = cgccpf;
		this.inscEstadual = inscEstadual;
	}

	public Integer getIdEntidade() {
		return idEntidade;
	}

	public void setIdEntidade(Integer idEntidade) {
		this.idEntidade = idEntidade;
	}

	public String getNomeEntidade() {
		return nomeEntidade;
	}

	public void setNomeEntidade(String nomeEntidade) {
		this.nomeEntidade = nomeEntidade;
	}

	public String getNomeFantasia() {
		return nomeFantasia;
	}

	public void setNomeFantasia(String nomeFantasia) {
		this.nomeFantasia = nomeFantasia;
	}

	public BigInteger getCgccpf() {
		return Cgccpf;
	}

	public void setCgccpf(BigInteger cgccpf) {
		Cgccpf = cgccpf;
	}

	public BigInteger getInscEstadual() {
		return inscEstadual;
	}

	public void setInscEstadual(BigInteger inscEstadual) {
		this.inscEstadual = inscEstadual;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((idEntidade == null) ? 0 : idEntidade.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Entidade other = (Entidade) obj;
		if (idEntidade == null) {
			if (other.idEntidade != null)
				return false;
		} else if (!idEntidade.equals(other.idEntidade))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Entidade [idEntidade=" + idEntidade + ", nomeEntidade=" + nomeEntidade + ", nomeFantasia="
				+ nomeFantasia + 
				", Cgccpf=" + Cgccpf + 
				", inscEstadual=" + inscEstadual + 
				"]";
	}
	
}